class AuthenticationController < ApplicationController
  
  # POST /auth/login
  def login
    @user = User.find_by_email(params[:email])
    if @user&.authenticate(params[:password])
      token = token_for(@user, @user.id)
      render json: { user: @user, token: token}, status: :ok
    else
      render json: { error: 'unauthorized User!, Please check email or password.' }, status: :unauthorized
    end
  end

  private

  def login_params
    params.permit(:email, :password)
  end

  def token_for(otp_record, user_id)
    JsonWebToken.encode(
      otp_record.id,
      24.hours.from_now,
      type: otp_record.class,
      user_id: user_id
    )
  end
end
