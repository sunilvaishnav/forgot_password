class PasswordController < ApplicationController
  before_action :validate_json_web_token
	
  def create
    if params['token'].present? && params['new_password'].present?
      otp = @token["type"].constantize.find(@token["id"])
      
      return render json: {errors: [{otp: 'OTP not validated'}]}, 
      status: :unprocessable_entity unless otp.activated?

      user = User.find(@token["user_id"])
      
      return render json: { errors: [{ password: "Password doesn't match" }]}, 
      status: :unprocessable_entity if !params['new_password'].eql?(params['password_confirmation'])
      
      if user.update(:password => params['new_password'], password_confirmation: params['password_confirmation'])
        otp.destroy
        render json: {user: user, meta: {message: "Password has changed."}}, status: :created
      else
        render json: { error: 'Password change failed'}, status: :unprocessable_entity
      end
    else
      return render json: { error: 'Token and new password are required'}, status: :unprocessable_entity
    end
  end
end
