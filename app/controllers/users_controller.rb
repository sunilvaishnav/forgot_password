class UsersController < ApplicationController
	before_action :validate_json_web_token, :current_user
	 
	def change_password 
		if current_user.authenticate(params[:old_password])
			current_user.update(password: params[:new_password])
			render json: { user: current_user, meta: {message: "Password has changed."}}, status: :ok
		else
			render json: { error: 'Invalid old password.' }, status: :unauthorized
		end
	end
end
