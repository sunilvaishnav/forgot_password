class ForgotPasswordController < ApplicationController
  before_action :validate_json_web_token, only: :varify_otp
	
  def create 
		@user = User.find_by_email(params[:email])

		return render json: { error: 'User not found' }, status: :not_found if @user.nil? 

    email_otp = EmailOtp.new(email: params[:email])
    if email_otp.save
    	token = token_for(email_otp, @user.id)
      render json: {email_otp: email_otp, token: token}, status: :created
    else
      render json: {
        errors: [email_otp.errors],
      }, status: :unprocessable_entity
    end
	end

	def varify_otp  
		if params[:otp].present? && params[:token].present?
      email_otp = @token["type"].constantize.find(@token["id"])
      
      return render json: {errors: [{otp: 'OTP must be a number'}]},
      status: :unprocessable_entity if params['otp'].to_s.count("a-zA-Z") > 0
    
      if email_otp.otp == params['otp'].to_i
        email_otp.activated = true
        email_otp.save
        render json: {
          messages: [{
            otp: 'OTP varifyed.',
          }, meta: {token: params['token']}],
        }, status: :ok
      else
        return render json: {
          errors: [{
            otp: 'Invalid OTP',
          }],
        }, status: :unprocessable_entity
      end
		else
			render json: {
        error: "OTP and Token must be required.",
      }, status: :unprocessable_entity
		end
	end

	private

	def token_for(otp_record, user_id)
    JsonWebToken.encode(
      otp_record.id,
      30.minutes.from_now,
      type: otp_record.class,
      user_id: user_id
    )
  end
end
