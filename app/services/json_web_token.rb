class JsonWebToken
  class << self

    def encode(id, data = {}, expiration = nil)
      expiration, data = data, expiration unless data.is_a?(Hash)
      data       ||= {}
      expiration ||= 24.hours.from_now

      payload = build_payload_for(id.to_i, data, expiration.to_i)

      JWT.encode payload, secret_key, algorithm
    end

    def decode(token)
      JWT.decode(token, secret_key, true, {
        :algorithm => algorithm,
      })[0]
    end

    private 

    def secret_key
      Rails.application.secrets.secret_key_base. to_s
    end

    def build_payload_for(id, data, expiration)
      {
        :id  => id,
        :exp => expiration,
      }.merge(data)
    end

    def algorithm
      'HS512'
    end
  end
end