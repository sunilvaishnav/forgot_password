class EmailOtpMailer < ApplicationMailer
  def otp_email
    @email_otp = params[:email_otp]
    @user = params[:user]
    
    mail(
      to: @email_otp.email,
      from: "from@example.com",
      subject: 'Email OTP') do |format|
        format.html { render 'otp_email' }
      end
  end
end
