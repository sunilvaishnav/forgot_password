class EmailOtp < ApplicationRecord
	validate :valid_email
  validates :email, presence: true

  before_create :generate_otp_and_valid_date
  after_create :send_email
  
  def generate_otp_and_valid_date
    self.otp         = rand(1_000..9_999)
    self.valid_until = Time.current + 5.minutes
  end

  private

  def valid_email
    unless email =~ URI::MailTo::EMAIL_REGEXP
      errors.add(:full_phone_number, "Invalid email format")
    end
  end

  def send_email 
    user = User.find_by(email: self.email)
    EmailOtpMailer.with(email_otp: self, user: user).otp_email.deliver
  end
end
