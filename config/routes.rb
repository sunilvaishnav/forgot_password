Rails.application.routes.draw do
  post '/auth/login', to: 'authentication#login'
  resources :forgot_password, only: :create do
    post :varify_otp, on: :collection
  end
  resources :password, only: :create
  post 'users/change_password', to: 'users#change_password'
end
